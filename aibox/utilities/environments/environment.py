import abc

__all__ = ['Environment']


class Environment(object, metaclass=abc.ABCMeta):
    __environment_manager = dict()

    def __init__(self, name=None):
        self._name = name if name else self.__class__.__name__
        self.__environment_manager.setdefault(self._name, 0)
        self.__environment_manager[self._name] += 1
        if self.__environment_manager[self._name] > 1:
            self._name = f'{self._name}' + str(self.__environment_manager[self._name])

    @abc.abstractmethod
    def reset(self):
        pass

    @abc.abstractmethod
    def step(self, *args, **kwargs):
        pass

    @property
    def obs_space(self):
        raise SyntaxError(f'Environment {self.__class__.__name__} has not defined observation space.')

    @property
    def act_space(self):
        raise SyntaxError(f'Environment {self.__class__.__name__} has not defined observation space.')
