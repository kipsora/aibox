from aibox.utilities.environments import Environment

__all__ = ['Gym']


class Gym(Environment):
    def _space_from_gym(self, space):
        from gym import spaces as gym_space
        import collections
        if isinstance(space, gym_space.Box):
            from aibox.utilities.spaces import BoxSpace
            return BoxSpace(space.shape, (space.low, space.high), space.dtype)
        elif isinstance(space, gym_space.Discrete):
            from aibox.utilities.spaces import DiscreteSpace
            return DiscreteSpace(list(range(space.n)), space.dtype)
        elif isinstance(space, collections.Iterable):
            from aibox.utilities.spaces import HybridSpace
            return HybridSpace(*[self._space_from_gym(sub_space) for sub_space in space])
        else:
            raise NotImplementedError

    def __init__(self, tag, render=False, name=None):
        super().__init__(name)
        import gym

        self._env = gym.make(tag)
        self._render = render
        self._obs_space = self._space_from_gym(self._env.observation_space)
        self._act_space = self._space_from_gym(self._env.action_space)

    def reset(self):
        return self._env.reset()

    def step(self, action):
        if self._render:
            self._env.render()
        return self._env.step(action)

    @property
    def obs_space(self):
        return self._obs_space

    @property
    def act_space(self):
        return self._act_space
