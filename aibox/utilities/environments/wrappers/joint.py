import numpy as np

from aibox.utilities import spaces
from aibox.utilities.environments import Environment

__all__ = ['FreeJointActionWrapper', 'InitializedJointActionWrapper', 'AbsoluteJointActionWrapper']


class FreeJointActionWrapper(Environment):
    @property
    def obs_space(self):
        return self._obs_space

    @property
    def act_space(self) -> spaces.BoxSpace:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.BoxSpace, steps=None, name=None):
        """
        Free Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly
        given by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliaries information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param steps: The number of steps the environment to be performed.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._output_space = output_space
        self._obs_space = output_space
        self._total_steps = steps if steps else 10
        self._act_space = spaces.BoxSpace(input_space.shape, (
            0, (input_space.max_bound - input_space.min_bound) / self._total_steps
        ))
        self._input_space = input_space

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None

    def reset(self):
        self._running_step = 0
        self._running_reward = 0
        self._running_action = self._act_space.zeros()
        return self._obs_space.zeros()

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        action = np.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action += action
        obs, actual_reward, info = self._f(self.current_parameters)
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        if isinstance(self._output_space, spaces.HybridSpace):
            obs = tuple(obs)
        return obs, incremental_reward, self._running_step >= self._total_steps, info

    @property
    def current_parameters(self):
        return self._running_action + self._input_space.min_bound


class InitializedJointActionWrapper(Environment):
    @property
    def obs_space(self):
        return self._obs_space

    @property
    def act_space(self) -> spaces.BoxSpace:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.BoxSpace, init_args, steps=None, name=None):
        """
        Free Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly
        given by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliaries information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param steps: The number of steps the environment to be performed.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._output_space = output_space
        self._obs_space = output_space
        self._total_steps = steps if steps else 10
        self._init_args = np.asarray(init_args)
        assert self._init_args.shape == input_space.shape
        assert (input_space.min_bound <= self._init_args).all() and (self._init_args <= input_space.max_bound).all()
        self._act_space = spaces.BoxSpace(input_space.shape, (
            (-self._init_args) / self._total_steps,
            (input_space.max_bound - input_space.min_bound - self._init_args) / self._total_steps
        ))
        self._input_space = input_space

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None

    def reset(self):
        self._running_step = 0
        self._running_reward = 0
        self._running_action = self._init_args.copy()
        return self._obs_space.zeros()

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        action = np.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action += action
        obs, actual_reward, info = self._f(self.current_parameters)
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        if isinstance(self._output_space, spaces.HybridSpace):
            obs = tuple(obs)
        return obs, incremental_reward, self._running_step >= self._total_steps, info

    @property
    def current_parameters(self):
        return self._running_action + self._input_space.min_bound


class AbsoluteJointActionWrapper(Environment):
    @property
    def obs_space(self):
        return self._obs_space

    @property
    def act_space(self) -> spaces.BoxSpace:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.BoxSpace, steps=None, name=None):
        """
        Free Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly
        given by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliaries information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param steps: The number of steps the environment to be performed.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._output_space = output_space
        self._total_steps = steps if steps else 10
        self._obs_space = spaces.combine(output_space, spaces.BoxSpace((self._total_steps + 1,)))
        self._act_space = input_space
        self._input_space = input_space
        self._indices = np.eye(self._total_steps + 1)

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None

    def reset(self):
        self._running_step = 0
        self._running_reward = 0
        self._running_action = self._act_space.sample()
        if isinstance(self._output_space, spaces.HybridSpace):
            return self._output_space.zeros() + (self._indices[self._running_step],)
        else:
            return self._output_space.zeros(), self._indices[self._running_step]

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        action = np.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action = action
        obs, actual_reward, info = self._f(self.current_parameters)
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        if isinstance(self._output_space, spaces.HybridSpace):
            obs = tuple(obs) + (self._indices[self._running_step],)
        else:
            obs = obs, self._indices[self._running_step]
        return obs, incremental_reward, self._running_step >= self._total_steps, info

    @property
    def current_parameters(self):
        assert self._running_step is not None
        return self._running_action.copy()
