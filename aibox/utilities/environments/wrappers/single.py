import numpy as np

from aibox.utilities import spaces
from aibox.utilities.environments import Environment

__all__ = ['SingularWrapper']


class SingularWrapper(Environment):
    @property
    def obs_space(self):
        return self._obs_space

    @property
    def act_space(self) -> spaces.BoxSpace:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.BoxSpace, info_length, name=None):
        """
        Free Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly
        given by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliaries information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._output_space = output_space
        self._info_length = info_length
        self._total_steps = int(np.prod(input_space.shape))
        self._obs_space = spaces.BoxSpace(self._total_steps)
        self._act_space = spaces.BoxSpace(spaces.SCALAR_SHAPE, (-1, 1))
        self._input_space = input_space

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None

    def reset(self):
        self._running_step = 0
        self._running_reward = 0
        self._running_action = self._input_space.zeros()
        return self._obs_space.zeros()

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        self._running_action[self._running_step - 1] = action
        one_hot_index = np.eye(self._total_steps)[self._running_step - 1]
        obs = one_hot_index
        if self._running_step < self._total_steps:
            return obs, 0, False, (None,) * self._info_length
        else:
            _, actual_reward, info = self._f(self.current_parameters)
            return obs, actual_reward, True, info

    @property
    def current_parameters(self):
        return self._input_space.denormalize(self._running_action)
