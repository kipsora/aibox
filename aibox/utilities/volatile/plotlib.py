def plot_xy(xs, ys, show=False, save_path=None, need_img_matrix=False, line_style='-'):
    if not show:
        import matplotlib as mpl
        mpl.use('Agg')
    import io
    import matplotlib.pyplot as plt
    import os
    import scipy.misc
    plt.close('all')
    for x, y in zip(xs, ys):
        plt.plot(x, y, ls=line_style)
    if save_path:
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        plt.savefig(save_path)
    if need_img_matrix:
        io_buffer = io.BytesIO()
        plt.savefig(io_buffer, format='png')
        io_buffer.seek(0)
        image = scipy.misc.imread(io_buffer)
        if show:
            plt.show()
        return image
    if show:
        plt.show()


def plot_two_xy(x, y1, y2, x_label=None, y1_label=None, y2_label=None,
                line_style1='-', line_style2='-',
                line_color1='#1f77b4', line_color2='#ff7f0e',
                title=None,
                show=False, save_path=None, need_img_matrix=False):
    if not show:
        import matplotlib as mpl
        mpl.use('Agg')
    import io
    import matplotlib.pyplot as plt
    import os
    import scipy.misc
    plt.close('all')

    fig, ax1 = plt.subplots()
    ax1.plot(x, y1, ls=line_style1, color=line_color1)
    x_label and ax1.set_xlabel(x_label)
    y1_label and ax1.set_ylabel(y1_label)

    ax2 = ax1.twinx()
    ax2.plot(x, y2, ls=line_style2, color=line_color2)
    y2_label and ax2.set_ylabel(y2_label)

    title and plt.title(title)

    fig.tight_layout()

    if save_path:
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        plt.savefig(save_path)
    if need_img_matrix:
        io_buffer = io.BytesIO()
        plt.savefig(io_buffer, format='png')
        io_buffer.seek(0)
        image = scipy.misc.imread(io_buffer)
        if show:
            plt.show()
        return image
    if show:
        plt.show()

