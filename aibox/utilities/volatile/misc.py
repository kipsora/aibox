def format_with_tag(data: dict):
    result = ''
    for k, v in data.items():
        if result:
            result += ' '
        result += k + ': '
        if not isinstance(v, int) and hasattr(v, '__float__'):
            result += '%5.3f' % float(v)
        elif hasattr(v, '__int__'):
            result += '%5d' % int(v)
        elif isinstance(v, dict):
            result += '(' + format_with_tag(v) + ')'
        else:
            raise NotImplementedError(str(type(v)) + ': ' + str(v))
    return result
