import numpy as np

__all__ = ['Statistic']


class _Result:
    pass


class Statistic(object):
    def __init__(self):
        self._values = dict()
        self._results = dict()

    def accumulate(self, **kwargs):
        for k, v in kwargs.items():
            self._values.setdefault(k, [])
            self._values[k].append(v)
            if k in self._results:
                self._results.pop(k)
        return self

    def __getattr__(self, item):
        if item in self._results:
            return self._results[item]
        if item not in self._values:
            self._values.setdefault(item, [])

        result = _Result()
        if len(self._values[item]):
            setattr(result, 'sum', np.sum(self._values[item]))
            setattr(result, 'mean', np.mean(self._values[item]))
            setattr(result, 'max', np.max(self._values[item]))
            setattr(result, 'min', np.min(self._values[item]))
            setattr(result, 'std', np.std(self._values[item]))
        else:
            setattr(result, 'sum', np.zeros(()))
            setattr(result, 'mean', np.zeros(()))
            setattr(result, 'std', np.zeros(()))
        self._results.setdefault(result)
        return result

    def reset(self):
        self._values.clear()
        self._results.clear()
        return self
