import json
import os
import shutil
import traceback

from aibox.utilities import misc

__all__ = ['Monitor']


class Monitor(object):
    """
    FIXME(kipsora): Monitor is neither multithreading safe nor multiprocessing safe.
    """
    def __init__(self, base_path, tags, name=None, logger=None, keep_files='AskOnError'):
        self._base_path = os.path.abspath(base_path)
        self._is_open = False
        self._keep_files = keep_files
        assert keep_files in ['OnError', 'All', 'None', 'AskOnError', 'Ask']
        self._tags = [tags] if isinstance(tags, str) else tags

        self._tag_paths = [self._base_path]
        for tag in self._tags:
            self._tag_paths.append(os.path.join(self._tag_paths[-1], tag))
        self._name = name if name else self._tags[0]

        self._common_path = self._tag_paths[-1]
        self._common_dump_path = os.path.join(self._common_path, '_dump')
        self._path = os.path.join(self._common_path, misc.get_unique_stamp())
        self._base_save_path = os.path.join(self._path, 'save')
        self._base_temp_path = os.path.join(self._path, 'temp')
        self._base_dump_path = os.path.join(self._path, 'dump')
        self._summary_path = self._path

        self._logger = logger

    def __enter__(self):
        if not self._logger:
            self.bind_logger(log_level='ERROR')
        os.makedirs(self._path, exist_ok=True)
        self.monitor_debug(f'Created base path {self._path}')
        os.makedirs(self._base_save_path)
        self.monitor_debug(f'Created save path {self._base_save_path}.')
        os.makedirs(self._base_dump_path)
        self.monitor_debug(f'Created dump path {self._base_dump_path}.')
        os.makedirs(self._base_temp_path)
        self.monitor_debug(f'Created temp path {self._base_temp_path}.')
        self._is_open = True
        self.monitor_info('Monitor successfully opened.')
        with open(os.path.join(self._path, 'monitor-status.json'), 'w') as writer:
            json.dump({'create_at': misc.get_stamp(), 'status': 'open'}, writer)
        return self

    def _remove_if_empty(self, path):
        if len(os.listdir(path)) == 0:
            shutil.rmtree(path, ignore_errors=True)
            self.monitor_debug(f'Removed path {path}')

    def _remove_all(self):
        shutil.rmtree(self._path, ignore_errors=True)
        self.monitor_debug(f'Removed path {self._path}')

    def _remove_unnecessary(self):
        shutil.rmtree(self._base_temp_path, ignore_errors=True)
        self.monitor_debug(f'Removed path {self._base_temp_path}')
        self._remove_if_empty(self._base_save_path)
        self._remove_if_empty(self._base_dump_path)
        self._remove_if_empty(self._path)

    def _remove_ask(self):
        while True:
            try:
                keep_files = input('Keep files (a)ll/(s)tandard/(n)one): ').lower()
                if keep_files == 'all' or keep_files == 'a':
                    self.monitor_info('Delete nothing due to user response.')
                    break
                elif keep_files == 'standard' or keep_files == 's':
                    self.monitor_info('Delete temporary files due to user response.')
                    self._remove_unnecessary()
                    break
                elif keep_files == 'none' or keep_files == 'n':
                    self.monitor_info('Delete all files due to user response.')
                    self._remove_all()
                    break
                else:
                    self.monitor_error(f'Cannot understand user response.')
            except:
                continue

    def dump_json(self, obj, file_name=None, is_common=False, *args, **kwargs):
        assert self._is_open
        if file_name is None:
            file_name = misc.get_stamp() + '.json'
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        file_path = os.path.join(dump_path, file_name)
        dirname = os.path.dirname(file_path)
        dirname and os.makedirs(dirname, exist_ok=True)
        with open(file_path, 'w') as writer:
            text = json.dumps(obj, *args, **kwargs)
            writer.write(text)
        self.monitor_info(f'Dumped to json file {os.path.join(self._base_dump_path, file_name)}')
        return text

    def load_json(self, file_name, is_common=False, *args, **kwargs):
        assert self._is_open
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        file_path = os.path.join(dump_path, file_name)
        dirname = os.path.dirname(file_path)
        dirname and os.makedirs(dirname, exist_ok=True)
        with open(file_path, 'r') as reader:
            result = json.load(reader, *args, **kwargs)
        self.monitor_info(f'Loaded from json file {os.path.join(self._base_dump_path, file_name)}')
        return result

    def dump_binary(self, obj, file_name=None, is_common=False):
        assert self._is_open
        if file_name is None:
            file_name = misc.get_stamp() + '.bin'
        from sklearn.externals import joblib
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        joblib.dump(obj, os.path.join(dump_path, file_name))
        self.monitor_info(f'Dumped to binary file {os.path.join(self._base_dump_path, file_name)}')

    def load_binary(self, file_name, is_common=False):
        assert self._is_open
        from sklearn.externals import joblib
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        result = joblib.load(os.path.join(dump_path, file_name))
        self.monitor_info(f'Loaded from binary file {os.path.join(self._base_dump_path, file_name)}')
        return result

    def is_exist(self, file_name, is_common=False):
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        file_name = os.path.join(dump_path, file_name)
        return os.path.exists(file_name)

    def remove(self, file_name, is_common=False):
        dump_path = self._common_dump_path if is_common else self._base_dump_path
        file_name = os.path.join(dump_path, file_name)
        os.remove(file_name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not os.path.exists(os.path.join(self._path, 'monitor-status.json')):
            os.makedirs(self._path, exist_ok=True)
            self.monitor_error('Cannot found monitor-status.json')
            with open(os.path.join(self._path, 'monitor-status.json'), 'w') as writer:
                json.dump({'created_at': None, 'status': 'open'}, writer)
        with open(os.path.join(self._path, 'monitor-status.json'), 'r') as handle:
            status = json.load(handle)
            assert isinstance(status, dict)
        with open(os.path.join(self._path, 'monitor-status.json'), 'w') as handle:
            status.update(dict(updated_at=misc.get_stamp(), status='closed'))
            json.dump(status, handle)

        self._is_open = False
        if exc_type is not None:
            exception = traceback.format_exc()
            exception = '\n'.join(['|  ' + s for s in exception.split('\n')[:-1]])
            exception = f'***\n{exception}\n***'
            self.monitor_error(f'An exception occured inside monitor:\n{exception}')
            if self._keep_files == 'AskOnError' or self._keep_files == 'Ask':
                self._remove_ask()
            elif self._keep_files == 'None':
                self._remove_all()
            elif self._keep_files != 'All':
                self._remove_unnecessary()
        else:
            if self._keep_files == 'None':
                self._remove_all()
            elif self._keep_files == 'Ask':
                self._remove_ask()
            elif self._keep_files != 'All':
                # AskOnError/OnError
                self._remove_unnecessary()
        self.monitor_info('Monitor successfully closed.')
        return True

    def get_base_temp_path(self):
        assert self._is_open
        return self._base_temp_path

    def get_base_save_path(self):
        assert self._is_open
        return self._base_save_path

    def get_summary_path(self):
        assert self._is_open
        return self._summary_path

    def get_new_path(self, rel_path):
        rel_path = os.path.join(self._path, rel_path)
        os.makedirs(rel_path, exist_ok=True)
        self.monitor_debug(f'Created new path: {rel_path}')
        return rel_path

    def console(self, message):
        print(f'{self._logger.name} ({misc.get_stamp()}) console: {message}')

    def monitor_info(self, message):
        if self._logger:
            self._logger.info(f'{self._logger.name} ({misc.get_stamp()}) INFO: {message}')

    def monitor_warning(self, message):
        if self._logger:
            self._logger.warning(f'{self._logger.name} ({misc.get_stamp()}) WARN: {message}')

    def monitor_error(self, message):
        if self._logger:
            self._logger.error(f'{self._logger.name} ({misc.get_stamp()}) ERROR: {message}')

    def monitor_debug(self, message):
        if self._logger:
            self._logger.debug(f'{self._logger.name} ({misc.get_stamp()}) DEBUG: {message}')

    def monitor_critical(self, message):
        if self._logger:
            self._logger.critical(f'{self._logger.name} ({misc.get_stamp()}) CRITICAL: {message}')

    @property
    def logger(self):
        return self._logger

    def set_log_level(self, level):
        if self._logger:
            self._logger.setLevel(level)
        return self

    def bind_logger(self, log_rel_path=None, log_stream='stderr', log_level='INFO', name=None):
        if self._logger:
            self.monitor_error('Cannot rebind logger')
        else:
            if log_rel_path is not None:
                if log_rel_path == 'default':
                    log_rel_path = os.path.join(self._path, log_rel_path or f'{self._name}.log')
            self._logger = misc.get_logger(name or f'aibox.monitor@{self._name}', log_rel_path, log_stream, log_level)
        return self

    def get_logger(self, name, log_rel_path=None, log_stream='stderr', log_level='INFO'):
        if log_rel_path:
            log_rel_path = os.path.join(self._path, log_rel_path)
        return misc.get_logger(name, log_rel_path, log_stream, log_level)
