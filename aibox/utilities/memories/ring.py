import numpy as np

from aibox.utilities.memories import Memory

__all__ = ['RingMemory']


class RingMemory(Memory):
    def __init__(self, max_size: int, entries: dict):
        super().__init__(max_size, entries)

        self._index = 0

    def store(self, **kwargs):
        self._place(self._index % self._max_size, **kwargs)
        self._index += 1

    def batch(self, batch_size, columns=None, alias=None):
        indices = np.random.choice(self.size, batch_size)
        return self.take(indices, columns, alias)

    @property
    def size(self):
        return min(self._index, self._max_size)
