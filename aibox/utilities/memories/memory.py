import abc
import numpy as np

from aibox.utilities import spaces

__all__ = ['Memory']


class Memory(object, metaclass=abc.ABCMeta):
    def __init__(self, max_size: int, entries: dict):
        self._buffer = dict()
        self._max_size = max_size
        for k, v in entries.items():
            if isinstance(v, spaces.SimpleSpace):
                self._buffer.setdefault(k, np.zeros(shape=[max_size] + list(v.shape), dtype=v.dtype))
            else:
                assert isinstance(v, spaces.HybridSpace)
                self._buffer.setdefault(k, tuple(np.zeros(shape=[max_size] + list(shape), dtype=dtype)
                                                 for shape, dtype in zip(v.shapes, v.dtypes)))

    @abc.abstractmethod
    def store(self, **kwargs):
        pass

    @abc.abstractmethod
    def batch(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def size(self):
        pass

    @property
    def max_size(self):
        return self._max_size

    def take(self, indices, columns, alias):
        if isinstance(columns, str):
            columns = [columns]
        if isinstance(alias, str):
            alias = [alias]
        if columns is not None:
            if alias is None:
                alias = columns
            values = {a: self._buffer[c] for c, a in zip(columns, alias)}
            return {
                k: tuple(i[indices] for i in v) if isinstance(v, tuple) else v[indices]
                for k, v in values.items()
            }
        else:
            return {
                k: tuple(i[indices] for i in v) if isinstance(v, tuple) else v[indices]
                for k, v in self._buffer.items()
            }

    def all(self, columns=None, alias=None, shuffle=True):
        indices = np.arange(self.size)
        if shuffle:
            np.random.shuffle(indices)
        return self.take(indices, columns, alias)

    def all_batch(self, batch_size, columns=None, alias=None):
        assert self.size > 0
        indices = np.arange(self.size)
        np.random.shuffle(indices)
        for i in range(0, self.size, batch_size):
            yield self.take(indices[i: i + batch_size], columns, alias)

    def _place(self, index, **kwargs):
        for k, current_buffer in self._buffer.items():
            v = kwargs[k]
            if isinstance(current_buffer, tuple):
                for buffer, value in zip(current_buffer, v):
                    buffer[index] = value
            else:
                assert isinstance(current_buffer, np.ndarray), current_buffer.__class__
                current_buffer[index] = v
