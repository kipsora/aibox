import numpy as np


class SumTree(object):
    """
    This SumTree code is a modified version and the original code is from:
    https://github.com/jaara/AI-blog/blob/master/SumTree.py
    Story data with its priority in the tree.
    """
    data_pointer = 0

    def __init__(self, max_size):
        self.capacity = max_size  # for all priority values
        self.tree = np.zeros(2 * max_size - 1)
        # [--------------Parent nodes-------------][-------leaves to recode priority-------]
        #             size: capacity - 1                       size: capacity
        self.data = np.zeros(max_size, dtype=object)  # for all transitions
        # [--------------data frame-------------]
        #             size: capacity

    def add(self, key, value):
        tree_idx = self.data_pointer + self.capacity - 1
        self.data[self.data_pointer] = value  # update data_frame
        self.update(tree_idx, key)  # update tree_frame

        self.data_pointer += 1
        if self.data_pointer >= self.capacity:  # replace when exceed the capacity
            self.data_pointer = 0

    def update(self, index, key):
        change = key - self.tree[index]
        self.tree[index] = key
        # then propagate the change through tree
        while index != 0:    # this method is faster than the recursive loop in the reference code
            index = (index - 1) // 2
            self.tree[index] += change

    def get_leaf(self, value):
        """
        Tree structure and array storage:
        Tree index:
             0         -> storing priority sum
            / \
          1     2
         / \   / \
        3   4 5   6    -> storing priority for transitions
        Array type for storing:
        [0,1,2,3,4,5,6]
        """
        parent_idx = 0
        while True:     # the while loop is faster than the method in the reference code
            cl_idx = 2 * parent_idx + 1         # this leaf's left and right kids
            cr_idx = cl_idx + 1
            if cl_idx >= len(self.tree):        # reach bottom, end search
                leaf_idx = parent_idx
                break
            else:       # downward search, always search for a higher priority node
                if value <= self.tree[cl_idx]:
                    parent_idx = cl_idx
                else:
                    value -= self.tree[cl_idx]
                    parent_idx = cr_idx

        data_idx = leaf_idx - self.capacity + 1
        return leaf_idx, self.tree[leaf_idx], self.data[data_idx]

    @property
    def sum_all(self):
        return self.tree[0]
