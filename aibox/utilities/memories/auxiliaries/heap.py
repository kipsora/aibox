import numpy as np


class Heap(object):
    def __init__(self, max_size):
        self._max_size = max_size
        self._size = 0

        self._rank = np.full((max_size + 1,), -np.inf, dtype=np.float32)
        self._slot = np.full((max_size + 1,), max_size, dtype=np.int64)  # The slot of the i-th element in the heap
        self._addr = np.full((max_size + 1,), max_size, dtype=np.int64)  # The addr of the i-th slot in the heap

    def add(self, rank, slot):
        assert self._size < self._max_size
        self._size += 1
        self._rank[self._size] = rank
        self._slot[self._size] = slot
        self._addr[slot] = self._size
        self._move_ancestor(self._size)

    def update(self, slot, rank):
        index = self._addr[slot]
        self._rank[index] = rank
        self._move_ancestor(index)
        self._move_children(index)

    def pop(self, index=1):
        assert self._size > 0
        rank, slot = self._rank[index], self._slot[index]
        if index == self._size:
            self._addr[self._size], self._slot[self._size], self._rank[self._size] = -1, -1, -np.inf
            self._size -= 1
        else:
            self._swap(index, self._size)
            self._addr[self._size], self._slot[self._size], self._rank[self._size] = -1, -1, -np.inf
            self._size -= 1
            self._move_children(index)
            self._move_ancestor(index)
        return rank, slot

    def top(self):
        return self._rank[1], self._slot[1]

    def get_from_addr(self, addr):
        return self._rank[addr], self._slot[addr]

    def get_from_slot(self, slot):
        return self._rank[self._addr[slot]], self._addr[slot]

    def _swap(self, a, b):
        self._rank[a], self._rank[b] = self._rank[b], self._rank[a]
        self._slot[a], self._slot[b] = self._slot[b], self._slot[a]
        self._addr[self._slot[a]], self._addr[self._slot[b]] = a, b

    def _move_ancestor(self, index):
        while index > 1:
            if self._rank[index] > self._rank[index >> 1]:
                self._swap(index, index >> 1)
                index >>= 1
                continue
            break

    def _move_children(self, index):
        # c > r > l: no action
        # c > l > r: no action
        # l > c > r: swap with l
        # r > c > l: swap with r
        # l > r > c: swap with l
        # r > l > c: swap with r
        while index << 1 <= self._size:
            cur, lch, rch = index, index << 1, index << 1 | 1
            if rch <= self._size and self._rank[rch] > max(self._rank[lch], self._rank[cur]):
                self._swap(cur, rch)
                index = rch
                continue
            if self._rank[lch] > self._rank[cur]:
                self._swap(cur, lch)
                index = lch
                continue
            break

    def empty(self):
        return self._size == 0

    @property
    def size(self):
        return self._size

    def debug(self, total=False):
        print('===== Heap Debug Tools =====')
        if total:
            print('rank:', self._rank)
            print('slot:', self._slot)
        else:
            print('rank:', self._rank[1: self._size + 1])
            print('slot:', self._slot[1: self._size + 1])

    def reconstruct(self):
        indices = np.argsort(-self._rank[1: self._size + 1]) + 1
        self._rank[1: self._size + 1] = self._rank[indices]
        self._slot[1: self._size + 1] = self._slot[indices]
        self._addr[self._slot[1: self._size + 1]] = np.arange(1, self._size + 1)
