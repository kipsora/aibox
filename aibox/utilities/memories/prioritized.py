import numpy as np

from aibox.utilities.memories import Memory
from aibox.utilities.memories.auxiliaries import Heap


class RankBasedPrioritizedMemory(Memory):
    # Prioritized Experience Replay Memory(rank-based version).
    # See https://arxiv.org/abs/1511.05952 for more details.

    def __init__(self, max_size: int, entries: dict, alpha=0.7):
        super().__init__(max_size, entries)

        self._pdf_raw = np.insert(np.power(np.arange(1, max_size + 1), -alpha), 0, 0)
        self._cdf_raw = np.cumsum(self._pdf_raw)
        # If current size is n we can then use self._pdf_raw[i] / self.cdf_raw[n] to get the probabilities
        self._index = 0
        self._heap = Heap(self._max_size)

    def store(self, **kwargs):
        self._index += 1
        self._index %= self._max_size
        self._place(self._index, **kwargs)

        if self._heap.size < self._max_size:
            self._heap.add(self._heap.top()[0], self._index)
        else:
            self._heap.update(self._index, self._heap.top()[0])

    def batch(self, batch_size, beta=None, columns=None, alias=None):
        if beta is None:
            indices = np.random.choice(self.size, batch_size)
            return self.take(indices, columns, alias)
        assert 0 <= beta <= 1
        size = self._heap.size
        uniform = np.random.rand(batch_size) * self._cdf_raw[size]
        ranks = np.searchsorted(self._cdf_raw[:size + 1], uniform)
        weights = np.power(self._pdf_raw[ranks] / self._cdf_raw[size], -beta)
        weights /= np.max(weights)
        _, slots = self._heap.get_from_addr(ranks)
        results = self.take(slots, columns, alias)
        results.update({'batch_weight': weights})
        return results, slots

    @property
    def size(self):
        return self._heap.size

    def update(self, slots, values):
        for slot, value in zip(slots, values):
            self._heap.update(slot, np.fabs(value))

    def reconstruct(self):
        self._heap.reconstruct()
