import abc

__all__ = ['Schema']


class Schema(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def reset(self):
        pass

    @abc.abstractmethod
    def next(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def value(self):
        pass
