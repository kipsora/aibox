from aibox.utilities.schemas.schema import Schema

__all__ = ['AdaptiveNoiseSchema']


class AdaptiveNoiseSchema(Schema):
    def __init__(self, init_value=0.1, adoption=1.01, threshold=0.01, adapt_step=10):
        super().__init__()
        self._adoption = adoption
        self._threshold = threshold
        self._init_value = init_value
        self._adapt_step = adapt_step

        self._running_value = None
        self._step_counter = None

        self.reset()

    def reset(self):
        self._running_value = self._init_value
        self._step_counter = 0
        return self

    def next(self, distance):
        self._step_counter += 1
        if self._step_counter % self._adapt_step == 0:
            if distance > self._threshold:
                self._running_value /= self._adoption
            else:
                self._running_value *= self._adoption
        return self._running_value

    @property
    def value(self):
        return self._running_value
