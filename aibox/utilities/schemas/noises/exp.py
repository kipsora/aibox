from aibox.utilities.schemas.schema import Schema

__all__ = ['ExponentialNoiseSchema']


class ExponentialNoiseSchema(Schema):
    def __init__(self, init_value=1.0, decay=0.9, decay_step=10, min_value=0.1):
        super().__init__()
        self._init_value = init_value
        self._decay = decay
        self._decay_step = decay_step
        self._min_value = min_value

        self._running_value = None
        self._step_counter = None

        self.reset()

    @property
    def value(self):
        return self._running_value

    def reset(self):
        self._running_value = self._init_value
        self._step_counter = 0
        return self

    def next(self):
        self._step_counter += 1
        if self._step_counter % self._decay_step == 0:
            result = self._running_value
            self._running_value = max(self._min_value, self._running_value * self._decay)
            return result
        else:
            return self._running_value
