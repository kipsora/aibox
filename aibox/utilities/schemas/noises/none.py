from aibox.utilities.schemas.schema import Schema

__all__ = ['NoneNoiseSchema']


class NoneNoiseSchema(Schema):
    def __init__(self):
        super().__init__()
        self._running_value = None
        self._step_counter = None

        self.reset()

    @property
    def value(self):
        return self._running_value

    def reset(self):
        self._running_value = 0
        self._step_counter = 0
        return self

    def next(self):
        self._step_counter += 1
        return self._running_value
