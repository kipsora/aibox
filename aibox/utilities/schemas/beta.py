from aibox.utilities.schemas.schema import Schema

__all__ = ['BetaSchema']


class BetaSchema(Schema):
    def __init__(self, init_value=0.5, increment=0.01, step_increment=10):
        super().__init__()
        self._init_value = init_value
        self._increment = increment
        self._step_increment = step_increment

        self._running_value = None
        self._step_counter = None

        self.reset()

    def reset(self):
        self._running_value = self._init_value
        self._step_counter = 0
        return self

    def next(self):
        self._step_counter += 1
        if self._step_counter % self._step_increment == 0:
            self._running_value = min(1.0, self._running_value + self._increment)
        return self._running_value

    @property
    def value(self):
        return self._running_value
