def broadcast(value, shape):
    import numpy as np
    if isinstance(value, int) or isinstance(value, float):
        value = np.ones(shape) * value
    else:
        value = np.asarray(value)
        assert value.shape == shape, (value.shape, shape)
    return value


def get_logger(name, log_path=None, log_stream='stderr', log_level='INFO'):
    import logging
    import sys
    import os

    logger = logging.getLogger(name)
    formatter = logging.Formatter()
    if log_stream:
        if log_stream in ['stdout', 'out', 'o']:
            log_stream = sys.stdout
        elif log_stream in ['stderr', 'err', 'e']:
            log_stream = sys.stderr
        else:
            assert NotImplementedError
        handler = logging.StreamHandler(stream=log_stream)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    if log_path:
        log_path = os.path.abspath(log_path)
        dirname = os.path.dirname(log_path)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        handler = logging.FileHandler(log_path)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    logger.setLevel(log_level)
    setattr(logger, 'log_path', log_path)
    return logger


def call_with_space(space, func, *args, keep_dims=False, **kwargs):
    from aibox.utilities import spaces

    assert isinstance(space, spaces.Space)
    if isinstance(space, spaces.SimpleSpace):
        return (func(*args, **kwargs),) if keep_dims else func(*args, **kwargs)
    else:
        assert isinstance(space, spaces.HybridSpace)
        keys, values = tuple(zip(kwargs.items())) if kwargs else tuple(), tuple()
        new_args = tuple(zip(*args)) if args else tuple(tuple() for _ in space.spaces)
        new_kwargs = tuple(dict(zip(keys, v)) for v in zip(values)) if kwargs else tuple(dict() for _ in space.spaces)
        assert len(new_args) == len(space.spaces) and len(new_kwargs) == len(space.spaces)
        return tuple(func(*args, **kwargs) for args, kwargs in zip(new_args, new_kwargs))


def convert_to_shape(shape):
    import numpy as np
    import collections
    if isinstance(shape, np.ndarray):
        return tuple(shape.tolist())
    elif isinstance(shape, int):
        return shape,
    elif isinstance(shape, collections.Iterable):
        return tuple(shape)
    else:
        raise NotImplementedError


def get_stamp():
    import datetime
    import time

    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S %f')


def set_static_variables(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


@set_static_variables(time_stamp=get_stamp())
def get_unique_stamp():
    return get_unique_stamp.time_stamp


def get_relative_path(path, current_path='./'):
    import os
    return os.path.relpath(os.path.abspath(path), os.path.abspath(current_path))
