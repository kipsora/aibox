import numpy as np

from aibox.utilities.spaces.simples import SimpleSpace

__all__ = ['DiscreteSpace']


class DiscreteSpace(SimpleSpace):
    def __init__(self, options, dtype=None):
        if isinstance(options, int):
            assert options > 0
            self._options = options
        else:
            self._options = tuple(options)
        super().__init__((), dtype if dtype else 'int64')

    def sample(self, size=1):
        if isinstance(self._options, int):
            return np.random.choice(self._options, size)
        else:
            return tuple(map(lambda x: self._options[x], np.random.choice(len(self._options), size).tolist()))

    def zeros(self):
        raise SyntaxError('Cannot get zeros from DiscreteSpace.')
