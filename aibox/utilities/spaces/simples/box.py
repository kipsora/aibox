import numpy as np

from aibox.utilities import misc
from aibox.utilities.spaces.simples import SimpleSpace

__all__ = ['BoxSpace']


class BoxSpace(SimpleSpace):
    def __init__(self, shape=(), bound=None, dtype=None):
        super().__init__(shape, dtype if dtype else 'float32')
        if bound is None:
            bound = np.inf
        if isinstance(bound, int) or isinstance(bound, float):
            bound = (-bound, bound)
        self._min_bound, self._max_bound = bound
        self._min_bound = misc.broadcast(self._min_bound, self._shape)
        self._max_bound = misc.broadcast(self._max_bound, self._shape)
        assert (self._min_bound < self._max_bound).all()

    def zeros(self):
        return np.zeros(shape=self._shape, dtype=self._dtype)

    def ones(self):
        return np.ones(shape=self._shape, dtype=self._dtype)

    @property
    def min_bound(self):
        return self._min_bound.copy()

    @property
    def max_bound(self):
        return self._max_bound.copy()

    @property
    def bound(self):
        return self.min_bound, self.max_bound

    def normalize(self, value, target_min=-1, target_max=1):
        assert self.is_finite
        result = (value - self._min_bound) / (self._max_bound - self._min_bound)
        result = result * (target_max - target_min) + target_min
        return result

    def denormalize(self, value, original_min=-1, original_max=1):
        assert self.is_finite
        result = (value - original_min) / (original_max - original_min)
        result = result * (self._max_bound - self._min_bound) + self._min_bound
        return result

    def sample(self):
        return self.denormalize(np.random.rand(*self._shape), original_min=0, original_max=1)

    @property
    def is_finite(self):
        return np.all(np.isfinite(self._min_bound)) and np.all(np.isfinite(self._max_bound))
