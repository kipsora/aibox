from abc import ABCMeta

from aibox.utilities import misc
from aibox.utilities.spaces import Space

__all__ = ['SimpleSpace']


class SimpleSpace(Space, metaclass=ABCMeta):
    def __init__(self, shape, dtype):
        super().__init__()

        self._shape = misc.convert_to_shape(shape)
        self._dtype = dtype

    @property
    def shape(self):
        return self._shape

    @property
    def dtype(self):
        return self._dtype
