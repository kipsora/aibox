from aibox.utilities.spaces import Space, SimpleSpace

__all__ = ['HybridSpace']


class HybridSpace(Space):
    def __init__(self, *sub_spaces: SimpleSpace):
        super().__init__()

        self._spaces = sub_spaces

    def sample(self):
        return tuple(space.sample() for space in self._spaces)

    @property
    def dtypes(self):
        return tuple(space.dtype for space in self._spaces)

    @property
    def shapes(self):
        return tuple(space.shape for space in self._spaces)

    @property
    def spaces(self):
        return self._spaces

    def zeros(self):
        return tuple(space.zeros() for space in self._spaces)
