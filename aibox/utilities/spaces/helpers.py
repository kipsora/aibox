from aibox.utilities import spaces

__all__ = ['combine', 'SCALAR_SHAPE', 'SCALAR_BOXSPACE']


def combine(*sub_spaces: spaces.Space):
    result = []
    for space in sub_spaces:
        if isinstance(space, spaces.SimpleSpace):
            result.append(space)
        else:
            assert isinstance(space, spaces.HybridSpace)
            result.extend(space.spaces)
    return spaces.HybridSpace(*result)


SCALAR_SHAPE = ()
SCALAR_BOXSPACE = spaces.BoxSpace(SCALAR_SHAPE)
