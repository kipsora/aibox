import abc

__all__ = ['Space']


class Space(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def sample(self):
        pass

    @abc.abstractmethod
    def zeros(self):
        pass
