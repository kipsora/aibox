import collections
import random

import numpy as np
import tensorflow as tf

from aibox.utilities import spaces

__all__ = ['get_placeholder_from_space',
           'get_variables_from_space',
           'get_session',
           'get_clipped_value_from_space',
           'get_single_value_dict_from_space',
           'get_value_dict_from_space',
           'set_seed',
           'set_tensorflow_verbosity']


def get_session(log_device_placement=False, allow_soft_placement=True, allow_gpu_growth=True):
    config = tf.ConfigProto(log_device_placement=log_device_placement, allow_soft_placement=allow_soft_placement)
    config.gpu_options.allow_growth = allow_gpu_growth
    return tf.Session(config=config)


def get_placeholder_from_space(space, batch_size=None, names=None):
    assert isinstance(space, spaces.Space)
    if isinstance(space, spaces.SimpleSpace):
        if isinstance(names, str):
            return tf.placeholder(space.dtype, shape=(batch_size,) + space.shape, name=names.format(''))
        elif names is None:
            return tf.placeholder(space.dtype, shape=(batch_size,) + space.shape)
        else:
            raise NotImplementedError
    else:
        assert isinstance(space, spaces.HybridSpace)
        if isinstance(names, str):
            return tuple(tf.placeholder(dtype, shape=(batch_size,) + shape, name=names.format(f'_{i}'))
                         for i, (dtype, shape) in enumerate(zip(space.dtypes, space.shapes)))
        elif isinstance(names, collections.Iterable):
            return tuple(tf.placeholder(dtype, shape=(batch_size,) + shape, name=name)
                         for dtype, shape, name in zip(space.dtypes, space.shapes, names))
        elif names is None:
            return tuple(tf.placeholder(dtype, shape=(batch_size,) + shape)
                         for dtype, shape in zip(space.dtypes, space.shapes))
        else:
            raise NotImplementedError


def get_variables_from_space(space, names, *args, **kwargs):
    assert isinstance(space, spaces.Space)
    if isinstance(space, spaces.SimpleSpace):
        if isinstance(names, str):
            return tf.get_variable(names.format(''), shape=space.shape, dtype=space.dtype, *args, **kwargs)
        else:
            raise NotImplementedError
    else:
        assert isinstance(space, spaces.HybridSpace)
        if isinstance(names, str):
            return tuple(tf.get_variable(names.format(f'_{i}'), dtype=dtype, shape=shape, *args, **kwargs)
                         for i, (dtype, shape) in enumerate(zip(space.dtypes, space.shapes)))
        elif isinstance(names, collections.Iterable):
            return tuple(tf.placeholder(name, dtype=dtype, shape=shape, *args, **kwargs)
                         for dtype, shape, name in zip(space.dtypes, space.shapes, names))
        else:
            raise NotImplementedError


def get_clipped_value_from_space(space, value, names=None):
    assert isinstance(space, spaces.Space)
    if isinstance(space, spaces.SimpleSpace):
        if isinstance(names, str):
            names = names.format('')
        elif isinstance(names, collections.Iterable):
            names = tuple(names)
            assert len(names) == 1
            names = names[0]
        elif names is not None:
            raise NotImplementedError
        if isinstance(space, spaces.BoxSpace):
            return tf.clip_by_value(value, *space.bound, name=names)
        else:
            return value
    else:
        assert isinstance(space, spaces.HybridSpace)
        if isinstance(names, str):
            return tuple(tf.clip_by_value(o, *space.bound, name=names.format(f'_{i}'))
                         if isinstance(space, spaces.BoxSpace) else o
                         for i, (o, space) in enumerate(zip(value, space.spaces)))
        elif isinstance(names, collections.Iterable):
            return tuple(tf.clip_by_value(o, *s.bound, name=name)
                         if isinstance(s, spaces.BoxSpace) else o
                         for o, s, name in zip(value, space.spaces, names))
        elif names is None:
            return tuple(tf.clip_by_value(o, *s.bound)
                         if isinstance(s, spaces.BoxSpace) else o
                         for o, s in zip(value, space.spaces))
        else:
            raise NotImplementedError


def set_seed(seed=None):
    if seed is None:
        seed = np.random.randint(0, (1 << 32) - 1)
    tf.set_random_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    return seed


def get_single_value_dict_from_space(space: spaces.Space, placeholder, value):
    if space is None:
        return {}
    assert placeholder is not None and value is not None
    if isinstance(space, spaces.SimpleSpace):
        return {placeholder: [value]}
    else:
        assert isinstance(space, spaces.HybridSpace)
        assert len(tuple(space.spaces)) == len(tuple(placeholder)) and len(tuple(value)) == len(tuple(space.spaces))
        return {k: [v] for k, v in zip(placeholder, value)}


def get_value_dict_from_space(space: spaces.Space, placeholder, value):
    if space is None:
        return {}
    assert placeholder is not None and value is not None
    if isinstance(space, spaces.SimpleSpace):
        return {placeholder: value}
    else:
        assert isinstance(space, spaces.HybridSpace)
        assert len(tuple(space.spaces)) == len(tuple(placeholder)) and len(tuple(value)) == len(tuple(space.spaces))
        return {k: v for k, v in zip(placeholder, value)}


def set_tensorflow_verbosity(verbosity='3'):
    import os
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = verbosity
