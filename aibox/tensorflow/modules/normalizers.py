import tensorflow as tf

from aibox.tensorflow import modules
from aibox.tensorflow.helpers import *
from aibox.utilities import misc

__all__ = ['Normalizer']


class Normalizer(modules.Module):
    def __init__(self, space, epsilon=1e-6, name=None):
        super().__init__(name)
        self._space = space
        self._epsilon = epsilon

        self._build()

    @modules.wrap_in_scope
    def _build(self):
        self._sum0 = get_variables_from_space(self._space, 'sum0_{}',
                                              initializer=tf.constant_initializer(self._epsilon), trainable=False)
        self._sum1 = get_variables_from_space(self._space, 'sum1_{}',
                                              initializer=tf.constant_initializer(), trainable=False)
        self._sum2 = get_variables_from_space(self._space, 'sum2_{}',
                                              initializer=tf.constant_initializer(self._epsilon), trainable=False)
        self._mean = misc.call_with_space(self._space, lambda sum1, sum0: sum1 / sum0, self._sum1, self._sum0)
        self.space = misc.call_with_space(self._space, lambda sum2, sum1, sum0, mean: tf.sqrt(
            tf.maximum(sum2 / sum0 - tf.square(mean), self._epsilon)), self._sum2, self._sum1, self._sum0, self._mean)
        self._std = self.space

    def get_update_op(self, value):
        op_update_sum0 = misc.call_with_space(
            self._space,
            lambda s, x: tf.assign_add(s, tf.reduce_sum(tf.ones_like(x), axis=0)),
            self._sum0, value, keep_dims=True
        )
        op_update_sum1 = misc.call_with_space(
            self._space,
            lambda s, x: tf.assign_add(s, tf.reduce_sum(x, axis=0)),
            self._sum1, value, keep_dims=True
        )
        op_update_sum2 = misc.call_with_space(
            self._space,
            lambda s, x: tf.assign_add(s, tf.reduce_sum(tf.square(x), axis=0)),
            self._sum2, value, keep_dims=True
        )
        return tuple(op_update_sum0 + op_update_sum1 + op_update_sum2)

    @property
    def std(self):
        return self._std

    @property
    def mean(self):
        return self._mean

    def normalize(self, value):
        return misc.call_with_space(self._space, lambda x, mean, std: (x - mean) / std,
                                    value, self._mean, self._std)

    def denormalize(self, value):
        return misc.call_with_space(self._space, lambda x, mean, std: x * std + mean,
                                    value, self._mean, self._std)
