import abc
import copy
import inspect

import tensorflow as tf

__all__ = ['Module', 'wrap_in_scope', 'assert_scope_built']


def wrap_in_scope(fn):
    if 'reuse' in inspect.signature(fn).parameters:
        def _call(self: 'Module', *args, reuse=False, **kwargs):
            scope = tf.variable_scope(self._name)
            with scope as new_scope:
                if reuse:
                    new_scope.reuse_variables()
                if not self._scope_name:
                    self._scope_name = new_scope.name
                assert self._scope_name == new_scope.name, (self._scope_name, new_scope.name)
                result = fn(self, *args, reuse=reuse, **kwargs)
                return result

        return _call
    else:
        def _call(self: 'Module', *args, reuse=False, **kwargs):
            scope = tf.variable_scope(self._name)
            with scope as new_scope:
                if reuse:
                    new_scope.reuse_variables()
                if not self._scope_name:
                    self._scope_name = new_scope.name
                assert self._scope_name == new_scope.name, (self._scope_name, new_scope.name)
                result = fn(self, *args, **kwargs)
                return result

        return _call


def assert_scope_built(fn):
    def _call(self: 'Module', *args, **kwargs):
        assert self._scope_name, f"You must build the `{self.__class__.__name__}' before call `{fn.__name__}'"
        return fn(self, *args, **kwargs)

    return _call


class Module(object, metaclass=abc.ABCMeta):
    __module_manager__ = dict()

    def __init__(self, name=None):
        self._name = name if name else self.__class__.__name__
        if self._name not in self.__module_manager__:
            self.__module_manager__.setdefault(self._name, 0)
        self.__module_manager__[self._name] += 1
        if self.__module_manager__[self._name] > 1:
            self._name = f'{self._name}{self.__module_manager__[self._name] - 1}'
        self._scope_name = None

    @assert_scope_built
    def get_variables(self, key):
        return tf.get_collection(key, self._scope_name)

    @property
    @assert_scope_built
    def trainable_variables(self):
        return self.get_variables(tf.GraphKeys.TRAINABLE_VARIABLES)

    @property
    @assert_scope_built
    def global_variables(self):
        return self.get_variables(tf.GraphKeys.GLOBAL_VARIABLES)

    @property
    def name(self):
        return self._name

    @property
    @assert_scope_built
    def var_scope_name(self):
        return self._scope_name

    def __copy__(self):
        newone = type(self).__new__(self.__class__)
        for k, v in self.__dict__.items():
            if isinstance(v, Module):
                setattr(newone, k, copy.copy(v))
            else:
                setattr(newone, k, v)
        newone._scope_name = None
        return newone
