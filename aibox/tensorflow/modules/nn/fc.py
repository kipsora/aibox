from aibox.tensorflow import modules

import tensorflow as tf

__all__ = ['MLPNetwork']


class MLPNetwork(modules.Module):
    def __init__(self,
                 units,
                 use_layer_norm=False,
                 use_batch_norm=False,
                 dropout_rate=None,
                 activation=tf.nn.relu,
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 bias_initializer=None,
                 bias_regularizer=None,
                 activity_regularizer=None,
                 use_bias=True,
                 training=None,
                 name=None):
        super().__init__(name=name)

        self._units = units
        self._use_layer_norm = use_layer_norm
        self._dropout_rate = dropout_rate
        self._use_batch_norm = use_batch_norm
        self._activation = activation
        self._kernel_initializer = kernel_initializer
        self._kernel_regularizer = kernel_regularizer
        self._bias_initializer = bias_initializer
        self._bias_regularizer = bias_regularizer
        self._activity_regularizer = activity_regularizer
        self._use_bias = use_bias
        self._training = training

    @modules.wrap_in_scope
    def apply(self, x, reuse=False, training=None, names=None):
        if len(x.shape) == 1:
            x = tf.expand_dims(x, axis=1)
        else:
            x = tf.layers.flatten(x)
        for index, unit in enumerate(self._units):
            name = names.format(f'_{index}') if names else None
            x = tf.layers.dense(x, units=unit,
                                kernel_initializer=self._kernel_initializer,
                                kernel_regularizer=self._kernel_regularizer,
                                bias_initializer=self._bias_initializer,
                                bias_regularizer=self._bias_regularizer,
                                activity_regularizer=self._activity_regularizer,
                                use_bias=self._use_bias,
                                reuse=reuse, name=name)
            if self._dropout_rate is not None:
                assert training is not None
                x = tf.layers.dropout(x, rate=self._dropout_rate, training=training)
            if self._use_batch_norm:
                raise NotImplementedError
            if self._use_layer_norm:
                raise NotImplementedError
            x = self._activation(x)
        return x
