import tensorflow as tf

from aibox.tensorflow import modules

__all__ = ['AutoAggressiveLSTMModule', 'FixedShapeStaticLSTMModule']


class FixedShapeStaticLSTMModule(modules.Module):
    def __init__(self, units, layers, name=None):
        super().__init__(name)

        self._units = units
        self._layers = layers

        self._cell = None

        assert self._layers > 0
        assert self._units > 0

    @property
    @modules.assert_scope_built
    def kernel_variables(self):
        return tuple(filter(lambda v: 'kernel' in v.name, self.trainable_variables))

    @property
    @modules.assert_scope_built
    def bias_variables(self):
        return tuple(filter(lambda v: 'bias' in v.name, self.trainable_variables))

    @staticmethod
    def __get_cell(units, name=None):
        return tf.nn.rnn_cell.BasicLSTMCell(num_units=units, name=name)

    @modules.wrap_in_scope
    def apply(self, x):
        """
        Build operations of FixedShapeStaticLSTMModule with input x
        :param x: The input tensor with shape [Batch, Sequence Length, Output Length]
        :return: The outputs and the internal state of LSTM
        """
        self._cell = self.__get_cell(self._units) if self._layers == 1 else \
            tf.nn.rnn_cell.MultiRNNCell(cells=[self.__get_cell(self._units) for _ in range(self._layers)])
        outputs, state = tf.nn.dynamic_rnn(self._cell, x, dtype=tf.float32)
        return outputs, state


class AutoAggressiveLSTMModule(modules.Module):
    def __init__(self, loops, name=None):
        super().__init__(name)

        self._loops = loops
        self._cell = None

    @staticmethod
    def __get_cell(units, name=None):
        return tf.nn.rnn_cell.BasicLSTMCell(num_units=units, name=name)

    @modules.wrap_in_scope
    def apply(self, lstm_state):
        """
        Build operations of FixedShapeStaticLSTMModule with input x
        :param lstm_state: The internal state of LSTM
        :return: The outputs of LSTM with the shape of [Batch, Sequence Length, Output Length] and the internal state
        """
        if lstm_state.__class__ is tuple:
            layers = len(lstm_state)
            internal_size = int(lstm_state[0].c.shape[1])
            input_shape = tf.shape(lstm_state[0].c)
        else:
            layers = 1
            internal_size = int(lstm_state.c.shape[1])
            input_shape = tf.shape(lstm_state.c)
        self._cell = self.__get_cell(internal_size) if layers == 1 else \
            tf.nn.rnn_cell.MultiRNNCell(cells=[self.__get_cell(internal_size) for _ in range(layers)])

        outputs_ta, lstm_state, _ = tf.nn.raw_rnn(
            self._cell,
            lambda t, pre_output, pre_state, _:
            (False, tf.zeros(input_shape), lstm_state, None, None) if pre_state is None else
            ((t >= self._loops), pre_output, pre_state, pre_output, None)
        )

        outputs = tf.reshape(outputs_ta.stack(), shape=[-1, self._loops, internal_size])
        return outputs, lstm_state
