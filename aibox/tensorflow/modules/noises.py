import abc

import tensorflow as tf

from aibox.tensorflow import modules

__all__ = ['Noise', 'SelectiveNoise', 'NormalNoise', 'SelectiveUniformNoise', 'UniformNoise', 'get_noise_from_name']


class Noise(modules.Module, metaclass=abc.ABCMeta):
    def __init__(self, shape, name=None):
        super().__init__(name)
        self._shape = shape

    @abc.abstractmethod
    def apply(self, batch_size, noise, value):
        pass


class NoneNoise(Noise):
    @modules.wrap_in_scope
    def apply(self, batch_size, noise, value):
        return value


class SelectiveNoise(Noise):
    @modules.wrap_in_scope
    def apply(self, batch_size, noise, value):
        return tf.where(tf.random_uniform((batch_size,) + self._shape) < noise,
                        2 * tf.random_uniform((batch_size,) + self._shape) - 1, value)


class UniformNoise(Noise):
    @modules.wrap_in_scope
    def apply(self, batch_size, noise, value):
        return value + (2 * tf.random_uniform((batch_size,) + self._shape) - 1) * noise


class NormalNoise(Noise):
    @modules.wrap_in_scope
    def apply(self, batch_size, noise, value):
        return value + tf.random_normal((batch_size,) + self._shape) * noise


class SelectiveUniformNoise(Noise):
    def __init__(self, shape, rate=0.5, name=None):
        super().__init__(shape, name)
        self._rate = rate

    @modules.wrap_in_scope
    def apply(self, batch_size, noise, value):
        return (tf.where(
            tf.random_uniform((batch_size,) + self._shape) < noise,
            tf.random_uniform((batch_size,) + self._shape) * 2 - 1,
            value
        ) + value) / 2


def get_noise_from_name(name: str, shape):
    lower_name = name.lower()
    if lower_name == 'none':
        return NoneNoise(shape)
    elif lower_name == 'selective':
        return SelectiveNoise(shape)
    elif lower_name == 'selectiveuniform':
        return SelectiveUniformNoise(shape)
    elif lower_name == 'uniform':
        return UniformNoise(shape)
    elif lower_name == 'normal':
        return NormalNoise(shape)
    else:
        raise NotImplementedError(f'Noise "{name}" is not implemented.')
