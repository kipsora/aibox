import tensorflow as tf

from aibox.tensorflow.helpers import get_session

__all__ = ['Tensorboard']


class Tensorboard(object):
    def __init__(self, logdir, session: tf.Session=None):
        self._logdir = logdir
        self._session = session if session else get_session()

        self._summary_ops = {}
        self._summary_inputs = {}
        self._summary_values = {}

    def write(self, name, value, stype):
        if name not in self._summary_inputs:
            tensor = tf.convert_to_tensor(value)
            self._summary_inputs.setdefault(name, tensor)
            if stype == 'scalar':
                self._summary_ops.setdefault(name, tf.summary.merge([tf.summary.scalar(name, tensor)]))
            elif stype == 'histogram':
                self._summary_ops.setdefault(name, tf.summary.merge([tf.summary.histogram(name, tensor)]))
            elif stype == 'text':
                self._summary_ops.setdefault(name, tf.summary.merge([tf.summary.text(name, tensor)]))
            elif stype == 'image':
                self._summary_ops.setdefault(
                    name, tf.summary.merge([tf.summary.image(name, tf.expand_dims(tensor, axis=0))]))
            elif stype == 'audio':
                self._summary_ops.setdefault(name, tf.summary.merge([tf.summary.audio(name, tensor)]))
            else:
                raise NotImplementedError
        self._summary_values.update({name: value})
        return self

    def write_scalar(self, value, name):
        return self.write(name, value, 'scalar')

    def write_scalars(self, values, names):
        for v, n in zip(values, names):
            self.write_scalar(v, n)

    def write_histogram(self, value, name):
        return self.write(name, value, 'histogram')

    def write_image(self, value, name):
        return self.write(name, value, 'image')

    def write_audio(self, value, name):
        return self.write(name, value, 'audio')

    def write_text(self, value, name, formatted=False):
        if formatted:
            value = value.replace('\n', '<br>').replace('    ', '&nbsp;&nbsp;')
        return self.write(name, value, 'text')

    def summary(self, global_step=None):
        feed_dict = dict((self._summary_inputs[k], self._summary_values[k]) for k in self._summary_values)
        all_summaries = self._session.run(tuple(self._summary_ops[k] for k in self._summary_values), feed_dict)
        for summary in all_summaries:
            self._writer.add_summary(summary, global_step)
        self._summary_values.clear()

    def __enter__(self):
        self._writer = tf.summary.FileWriter(self._logdir, self._session.graph)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._writer.close()
        self._writer = None
