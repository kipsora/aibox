import abc
import logging
import os

import tensorflow as tf

from aibox.tensorflow.helpers import *
from aibox.utilities import misc

__all__ = ['Model']


class Model(object, metaclass=abc.ABCMeta):
    __model_manager__ = dict()

    def __init__(self,
                 session: tf.Session = None,
                 graph: tf.Graph = None,
                 logger: logging.Logger = None,
                 name: str = None):
        self._graph = graph if graph else tf.get_default_graph()
        self._name = name if name else self.__class__.__name__
        if self._name not in self.__model_manager__:
            self.__model_manager__.setdefault(self._name, 0)
        self.__model_manager__[self._name] += 1
        if self.__model_manager__[self._name] > 1:
            self._name = f'{self._name}{self.__model_manager__[self._name] - 1}'
        self._logger = logger if logger else misc.get_logger(self._name)
        self._session = session if session else get_session()

    @abc.abstractmethod
    def get_variables(self, key):
        pass

    @property
    def trainable_variables(self):
        return self.get_variables(tf.GraphKeys.TRAINABLE_VARIABLES)

    @property
    def global_variables(self):
        return self.get_variables(tf.GraphKeys.GLOBAL_VARIABLES)

    @property
    def saveable_variables(self):
        return self.get_variables(tf.GraphKeys.GLOBAL_VARIABLES) + self.get_variables(tf.GraphKeys.SAVEABLE_OBJECTS)

    def save(self, path, step=None, pickleable=None):
        self._logger.debug(f'D: Saving step {step} to "{path}"...')
        if not os.path.exists(path):
            os.makedirs(path)
        saver = tf.train.Saver(var_list=self.saveable_variables)
        saver.save(self._session, path, global_step=step)
        if not pickleable:
            from sklearn.externals import joblib
            dump_path = os.path.join(path, f'data{step}.pickle')
            self._logger.debug(f'D: Dumping pickle object to {dump_path}...')
            joblib.dump(pickleable, dump_path)
        self._logger.info(f'I: Successfully saved step {step} to "{path}"".')

    def load(self, path, step=None):
        self._logger.debug(f'D: Loading step {step} from "{path}"...')
        saver = tf.train.Saver()
        saver.restore(self._session, save_path=path)
        self._logger.info(f'I: Successfully loaded step {step} from "{path}"".')

    @property
    def graph(self):
        return self._graph

    @property
    def session(self):
        return self._session

    @property
    def logger(self):
        return self._logger

    @property
    def name(self):
        return self._name
