import logging

import tensorflow as tf

from aibox.tensorflow.helpers import *
from aibox.tensorflow.models.model import Model
from aibox.tensorflow.models.rl.ddpg import *
from aibox.tensorflow.modules import *
from aibox.utilities import spaces

__all__ = ['DeepDPG']


class DeepDPG(Model):
    def __init__(self,
                 obs_space: spaces.Space,
                 act_space: spaces.BoxSpace,
                 actor: Actor = None,
                 critic: Critic = None,
                 use_obs_norm=False,
                 use_val_norm=False,
                 use_pop_art=False,
                 use_param_noise=False,
                 use_batch_weight=False,
                 action_noise='SelectiveUniform',
                 gamma=0.9,
                 session: tf.Session = None,
                 graph: tf.Graph = None,
                 logger: logging.Logger = None,
                 name=None):
        super().__init__(session, graph, logger, name)

        self._obs_space = obs_space
        self._act_space = act_space

        self._use_obs_norm = use_obs_norm
        self._use_val_norm = use_val_norm
        self._use_pop_art = use_pop_art
        self._use_param_noise = use_param_noise
        self._use_batch_weight = use_batch_weight

        self._actor = actor if actor else MLPActor(self._obs_space, self._act_space)
        self._critic = critic if critic else MLPCritic(self._obs_space)
        self._target_actor = self._actor.copy('Target')
        self._target_critic = self._critic.copy('Target')

        with self._graph.as_default(), tf.variable_scope(self._name) as scope:
            self._scope_name = scope.name
            self._batch_size = tf.placeholder(tf.int64, shape=(), name='batch_size')
            obs0 = self._obs0 = get_placeholder_from_space(obs_space, names='obs0{}')
            obs1 = self._obs1 = get_placeholder_from_space(obs_space, names='obs1{}')
            self._action = get_placeholder_from_space(act_space, names='action{}')
            self._reward = tf.placeholder(tf.float32, shape=(None,), name='reward')
            self._terminate = tf.placeholder(tf.bool, shape=(None,), name='terminal')
            self._tau = tf.placeholder(tf.float32, shape=(), name='tau')
            self._anoise = tf.placeholder(tf.float32, shape=(), name='action_noise')
            self._actor_lr = tf.placeholder(tf.float32, shape=(), name='actor_lr')
            self._critic_lr = tf.placeholder(tf.float32, shape=(), name='critic_lr')
            if use_batch_weight:
                self._batch_weight = tf.placeholder(tf.float32, shape=(None,), name='batch_weight')

            action_noise = get_noise_from_name(action_noise, self._act_space.shape)

            if use_obs_norm:
                self._obs_norm = Normalizer(self._obs_space)
                obs0 = self._obs_norm.normalize(obs0)
                obs1 = self._obs_norm.normalize(obs1)
                self._op_update_obs_normalizer = self._obs_norm.get_update_op(self._obs0)

            obs0 = get_clipped_value_from_space(self._obs_space, obs0, 'clip_obs0{}')
            obs1 = get_clipped_value_from_space(self._obs_space, obs1, 'clip_obs1{}')

            self._norm_action = self._act_space.normalize(self._action)
            self._norm_actor_action = self._actor.apply(obs0)
            self._norm_target_action = self._target_actor.apply(obs1)

            if use_val_norm or use_pop_art:
                self._val_norm = Normalizer(spaces.SCALAR_BOXSPACE)

                norm_target_val = self._target_critic.apply(obs1, self._norm_target_action)
                target_val = self._val_norm.denormalize(norm_target_val)

                self._norm_critic_val = self._critic.apply(obs0, self._norm_action)
                self._norm_critic_actor_val = self._critic.apply(obs0, self._norm_actor_action, reuse=True)

                self._critic_val = self._val_norm.denormalize(self._norm_critic_val)
                self._critic_actor_val = self._val_norm.denormalize(self._norm_critic_actor_val)

                self._target = tf.where(self._terminate, self._reward, self._reward + gamma * target_val)
                self._norm_target = self._val_norm.normalize(self._target)

                self._op_update_val_normalizer = self._val_norm.get_update_op(self._target)

                self._critic_loss = tf.square(self._norm_target - self._norm_critic_val)
                self._actor_loss = -self._critic_actor_val

                if use_pop_art:
                    # See https://arxiv.org/abs/1602.07714. It is better to combine critic value optimization with
                    # POP-ART which can be compensated for the bias brought by the value normalization
                    self._old_val_norm_mean = tf.placeholder(tf.float32, name='old_val_norm_mean')
                    self._old_val_norm_std = tf.placeholder(tf.float32, name='old_val_norm_std')
                    self._op_train_popart = []
                    for i in (self._critic, self._target_critic):
                        k, b = i.popart_variables
                        real_b = b * self._old_val_norm_std + self._old_val_norm_mean
                        self._op_train_popart.append(tf.assign(k, k * self._old_val_norm_std / self._val_norm.std))
                        self._op_train_popart.append(tf.assign(b, (real_b - self._val_norm.mean) / self._val_norm.std))
            else:
                target_val = self._target_critic.apply(obs1, self._norm_target_action)
                self._critic_val = self._critic.apply(obs0, self._norm_action)
                self._critic_actor_val = self._critic.apply(obs0, self._norm_actor_action, reuse=True)
                self._target = tf.where(self._terminate, self._reward, self._reward + gamma * target_val, name='target')
                self._critic_loss = tf.square(self._target - self._critic_val)
                self._actor_loss = -self._critic_actor_val

            if use_batch_weight:
                self._critic_loss *= self._batch_weight
                self._actor_loss *= self._batch_weight
            self._critic_loss = tf.reduce_mean(self._critic_loss, name='critic_loss')
            self._actor_loss = tf.reduce_mean(self._actor_loss, name='actor_loss')

            if use_param_noise:
                # See https://arxiv.org/abs/1706.01905
                self._pnoise = tf.placeholder(tf.float32, name='param_noise_std')
                self._pnoise_actor = self._actor.copy('ParamNoise')
                norm_noisy_actor_action = tf.clip_by_value(action_noise.apply(
                    self._batch_size, self._anoise, self._pnoise_actor.apply(obs0)), -1, 1)

                self._op_build_pnoise_actor = [
                    tf.assign(n, s + tf.random_normal(tf.shape(s)) * self._pnoise)
                    if s in self._actor.perturbable_variables else tf.assign(n, s)
                    for n, s in zip(self._pnoise_actor.global_variables, self._actor.global_variables)
                ]
                self._adapt_pnoise_actor = self._actor.copy('AdaptiveParamNoise')
                norm_adapt_pnoise_actor_action = self._adapt_pnoise_actor.apply(obs0)
                self._op_build_adapt_pnoise_actor = [
                    tf.assign(n, s + tf.random_normal(tf.shape(s)) * self._pnoise)
                    if s in self._actor.perturbable_variables else tf.assign(n, s)
                    for n, s in zip(self._adapt_pnoise_actor.global_variables, self._actor.global_variables)
                ]
                self._adapt_pnoise_distance = tf.reduce_mean(tf.square(
                    norm_adapt_pnoise_actor_action - self._norm_actor_action))
            else:
                norm_noisy_actor_action = tf.clip_by_value(action_noise.apply(
                    self._batch_size, self._anoise, self._norm_actor_action), -1, 1)

            self._noisy_actor_action = self._act_space.denormalize(norm_noisy_actor_action)
            norm_critic_noisy_actor_value = self._critic.apply(obs0, norm_noisy_actor_action, reuse=True)
            self._critic_noisy_actor_value = self._val_norm.denormalize(norm_critic_noisy_actor_value) \
                if use_val_norm or use_pop_art else norm_critic_noisy_actor_value

            with tf.variable_scope('Target/Update'):
                self._op_build_target_actor = [tf.assign(t, s) for t, s in zip(
                    self._target_actor.global_variables, self._actor.global_variables)]
                self._op_build_target_critic = [tf.assign(t, s) for t, s in zip(
                    self._target_critic.global_variables, self._critic.global_variables)]
                self._op_move_target_actor = [tf.assign(t, t * (1 - self._tau) + s * self._tau) for t, s in zip(
                    self._target_actor.global_variables, self._actor.global_variables)]
                self._op_move_target_critic = [tf.assign(t, t * (1 - self._tau) + s * self._tau) for t, s in zip(
                    self._target_critic.global_variables, self._critic.global_variables)]

            actor_optimizer = tf.train.AdamOptimizer(self._actor_lr, name='ActorOptimizer')
            critic_optimizer = tf.train.AdamOptimizer(self._critic_lr, name='CriticOptimizer')
            self._op_train_critic = critic_optimizer.minimize(
                self._critic_loss, var_list=self._critic.trainable_variables, name='critic_grad')
            self._op_train_actor = actor_optimizer.minimize(
                self._actor_loss, var_list=self._actor.trainable_variables, name='actor_grad')

        self.reset()

    def get_variables(self, key):
        with self._graph.as_default():
            return tf.get_collection(key, self._scope_name)

    def reset(self):
        self._session.run(tf.variables_initializer(self.get_variables(tf.GraphKeys.GLOBAL_VARIABLES)))
        self._session.run((self._op_build_target_actor, self._op_build_target_critic))

    def get_memory_entries(self):
        entries = dict(obs0=self._obs_space, obs1=self._obs_space, action=self._act_space,
                       reward=spaces.SCALAR_BOXSPACE,
                       terminate=spaces.DiscreteSpace([True, False], 'bool'))
        return entries

    def infer(self, obs, anoise, with_critic_values=False):
        feed_dict = {
            self._batch_size: 1,
            self._anoise: anoise
        }
        feed_dict.update(get_single_value_dict_from_space(self._obs_space, self._obs0, obs))

        required_list = [self._noisy_actor_action]
        if with_critic_values:
            required_list.append(self._critic_actor_val)
            required_list.append(self._critic_noisy_actor_value)
        values = self._session.run(required_list, feed_dict)
        results = []
        if with_critic_values:
            results.append(values.pop()[0])
            results.append(values.pop()[0])
        results.append(values.pop()[0])
        results = tuple(reversed(results))
        if len(results) == 1:
            return results[0]
        return results

    def update_obs_normalizer(self, obs):
        assert self._use_obs_norm
        self._session.run(self._op_update_obs_normalizer,
                          get_single_value_dict_from_space(self._obs_space, self._obs0, obs))

    def train(self, obs0, obs1, action, reward, terminate,
              actor_lr=1e-3, critic_lr=1e-3, tau=1e-2, batch_weight=None, need_target=False):
        feed_dict = {
            self._action: action,
            self._reward: reward,
            self._terminate: terminate,
            self._actor_lr: actor_lr,
            self._critic_lr: critic_lr,
            self._batch_size: len(terminate)
        }
        feed_dict.update(get_value_dict_from_space(self._obs_space, self._obs0, obs0))
        feed_dict.update(get_value_dict_from_space(self._obs_space, self._obs1, obs1))
        if self._use_batch_weight:
            feed_dict.setdefault(self._batch_weight, batch_weight)
        else:
            assert batch_weight is None

        if self._use_val_norm and self._use_pop_art:
            old_val_norm_mean, old_val_norm_std = self._session.run((self._val_norm.mean, self._val_norm.std))
            self._session.run(self._op_update_val_normalizer, feed_dict)
            self._session.run(self._op_train_popart, feed_dict={
                self._old_val_norm_mean: old_val_norm_mean,
                self._old_val_norm_std: old_val_norm_std
            })
        elif self._use_val_norm:
            self._session.run(self._op_update_val_normalizer, feed_dict)

        _, critic_loss = self._session.run((self._op_train_critic, self._critic_loss), feed_dict)
        _, actor_loss = self._session.run((self._op_train_actor, self._actor_loss), feed_dict)
        self._session.run((self._op_move_target_critic, self._op_move_target_actor), feed_dict={self._tau: tau})
        if need_target:
            return critic_loss, actor_loss, self._session.run(self._target, feed_dict)
        else:
            return critic_loss, actor_loss

    def get_pnoise_distance(self, obs, pnoise):
        assert self._use_param_noise
        self._session.run(self._op_build_adapt_pnoise_actor, feed_dict={self._pnoise: pnoise})
        feed_dict = get_value_dict_from_space(self._obs_space, self._obs0, obs)
        return self._session.run(self._adapt_pnoise_distance, feed_dict)

    def set_pnoise(self, pnoise):
        assert self._use_param_noise
        self._session.run(self._op_build_pnoise_actor, feed_dict={self._pnoise: pnoise})

    @property
    def obs_norm_values(self):
        import numpy as np
        assert self._use_obs_norm
        mean, std = self._session.run((self._obs_norm.mean, self._obs_norm.std))
        if isinstance(self._obs_space, spaces.HybridSpace):
            total = 0
            total_sum_mean = 0
            total_sum_std = 0
            for space, m, s in zip(self._obs_space.spaces, mean, std):
                total += np.prod(space.shape)
                total_sum_mean += np.sum(m)
                total_sum_std += np.sum(s)
            return total_sum_mean / total, total_sum_std / total
        else:
            return np.mean(mean), np.mean(std)

    @property
    def val_norm_values(self):
        import numpy as np
        assert self._use_val_norm or self._use_pop_art
        mean, std = self._session.run((self._val_norm.mean, self._val_norm.std))
        return np.mean(mean), np.mean(std)
