import abc
import copy
import numpy as np

from aibox.tensorflow import modules
import tensorflow as tf

from aibox.tensorflow.modules import nn
from aibox.utilities import spaces, misc

__all__ = ['Actor', 'MLPActor', 'FixedShapeRNNJointActor']


class Actor(modules.Module, metaclass=abc.ABCMeta):
    def __init__(self, name=None):
        super().__init__(name=name)

    def copy(self, prefix):
        result = copy.copy(self)
        modules.Module.__init__(result, prefix + '/' + self._name)
        return result

    @property
    @abc.abstractmethod
    def perturbable_variables(self):
        pass

    @abc.abstractmethod
    def apply(self, *args, **kwargs):
        pass


class MLPActor(Actor):
    def __init__(self, obs_space: spaces.Space, act_space: spaces.BoxSpace,
                 space_units=None, name=None):
        super().__init__(name=name)

        self._obs_space = obs_space
        self._act_space = act_space
        self._space_units = space_units if space_units else (300, 300, 300)
        self._space_networks = misc.call_with_space(
            self._obs_space, lambda: nn.MLPNetwork(units=self._space_units))

    @modules.wrap_in_scope
    def apply(self, obs):
        result = misc.call_with_space(
            self._obs_space, lambda network, x: network.apply(x), self._space_networks, obs)
        if isinstance(self._obs_space, spaces.HybridSpace):
            result = tf.concat(result, axis=1)
        result = tf.layers.dense(result, units=np.prod(self._act_space.shape))
        result = tf.nn.tanh(result)
        result = tf.reshape(result, (-1,) + self._act_space.shape)
        return result

    @property
    @modules.assert_scope_built
    def perturbable_variables(self):
        return tuple(filter(lambda v: 'LayerNorm' not in v.name, self.trainable_variables))

    def copy(self, prefix):
        result = super(MLPActor, self).copy(prefix)
        result._space_networks = misc.call_with_space(
            self._obs_space, lambda: nn.MLPNetwork(units=self._space_units))
        return result


class FixedShapeRNNJointActor(Actor):
    def __init__(self, loops, preprocessor, postprocessor, tanh_scale=1, encoder=None, decoder=None, name=None):
        super().__init__(name)

        self._encoder = encoder if encoder else nn.FixedShapeStaticLSTMModule(units=256, layers=3)
        self._decoder = decoder if decoder else nn.AutoAggressiveLSTMModule(loops=loops)
        self._preprocessor = preprocessor
        self._postprocessor = postprocessor
        self._tanh_scale = tanh_scale

    @modules.wrap_in_scope
    def apply(self, obs):
        obs = self._preprocessor(obs)
        with tf.variable_scope('Encoder'):
            _, encoder_state = self._encoder.apply(obs)

        with tf.variable_scope('Decoder'):
            decoder_outputs, _ = self._decoder.apply(encoder_state)

        decoder_outputs = self._postprocessor(decoder_outputs)
        decoder_outputs = tf.nn.tanh(decoder_outputs / self._tanh_scale)

        return decoder_outputs

    @property
    @modules.assert_scope_built
    def perturbable_variables(self):
        return self.trainable_variables
