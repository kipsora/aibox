import abc
import copy

import tensorflow as tf

from aibox.tensorflow import modules
from aibox.tensorflow.modules import nn
from aibox.utilities import misc, spaces

__all__ = ['Critic', 'MLPCritic']


class Critic(modules.Module, metaclass=abc.ABCMeta):
    def __init__(self, name=None):
        super().__init__(name)

    def copy(self, prefix):
        result = copy.copy(self)
        modules.Module.__init__(result, prefix + '/' + self._name)
        return result

    @abc.abstractmethod
    def apply(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def popart_variables(self):
        pass


class MLPCritic(Critic):
    def __init__(self, obs_space: spaces.Space, obs_units=None, act_units=None, common_units=None, name=None):
        super().__init__(name)
        self._obs_space = obs_space
        self._obs_units = obs_units if obs_units else (1024, 512)
        self._act_units = act_units if act_units else (256, 256)
        self._common_units = common_units if common_units else (256,)

        self._obs_networks = misc.call_with_space(self._obs_space, lambda: nn.MLPNetwork(self._obs_units))
        self._act_network = nn.MLPNetwork(self._act_units)
        self._common_network = nn.MLPNetwork(self._common_units)

        self._popart_variables = None

    @modules.wrap_in_scope
    def apply(self, obs, act, reuse=False):
        x = misc.call_with_space(self._obs_space, lambda network, o: network.apply(o),
                                 self._obs_networks, obs, keep_dims=True)
        y = self._act_network.apply(act)
        h = tf.concat(x + (y,), axis=1)
        h = self._common_network.apply(h)
        layer = tf.layers.Dense(units=1)
        h = tf.reshape(layer(h), shape=(-1,))
        if self._popart_variables is None:
            self._popart_variables = (layer.kernel, layer.bias)
        return h

    @property
    @modules.assert_scope_built
    def popart_variables(self):
        return self._popart_variables

    def copy(self, prefix):
        result = super(MLPCritic, self).copy(prefix)
        result._obs_networks = misc.call_with_space(self._obs_space, lambda: nn.MLPNetwork(self._obs_units))
        result._act_network = nn.MLPNetwork(self._act_units)
        result._common_network = nn.MLPNetwork(self._common_units)
        return result
