import time

from aibox.utilities import spaces
from aibox.utilities.memories.prioritized import RankBasedPrioritizedMemory

memory = RankBasedPrioritizedMemory(1000, {'a': spaces.BoxSpace()})

for i in range(100000):
    memory.store(a=3)

start = time.time()
print(memory.batch(131072, 0.3))
print(time.time() - start)
