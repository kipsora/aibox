import numpy as np

import aibox
import aibox.tensorflow as tfbox

with tfbox.Tensorboard(logdir='./') as summary:
    for i in range(10000):
        print(i)
        xs = np.arange(-np.pi, np.pi, 0.01)
        ys = np.sin(xs) + np.random.rand(*xs.shape)
        image = aibox.utilities.volatile.plotlib.plot_xy([xs], [ys], need_img_matrix=True)
        summary.write_image(image, 'graph')
        summary.summary(i)
