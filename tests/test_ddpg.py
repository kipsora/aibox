import numpy as np
from collections import deque

import aibox
import aibox.tensorflow as tfbox

env = aibox.utilities.environments.Gym('Pendulum-v0')
model = tfbox.models.ddpg.DeepDPG(obs_space=env.obs_space, act_space=env.act_space, action_noise='SelectiveUniform',
                                  use_val_norm=True, use_obs_norm=False, use_pop_art=False, use_param_noise=False,
                                  use_batch_weight=False)
memory = aibox.utilities.memories.RingMemory(max_size=100000, entries=model.get_memory_entries())
anoise = aibox.utilities.schemas.LinearNoiseSchema(min_value=0)
# pnoise = aibox.utilities.schemas.AdaptiveNoiseSchema()
beta = aibox.utilities.schemas.BetaSchema()

summary = tfbox.Tensorboard(logdir='./', session=model.session)
summary.__enter__()
anoise.reset()
# pnoise.reset()
beta.reset()
model.reset()
queue = deque(maxlen=50)
for i in range(2000):
    obs0 = env.reset()
    terminate = False
    current_anoise = anoise.next()
    reward_sum = 0
    # model.update_obs_normalizer(obs0)

    # if memory.size > 128:
    #     distance = model.get_pnoise_distance(**memory.batch(128, columns='obs0', alias='obs'), pnoise=pnoise.current)
    #     model.set_pnoise(pnoise.next(distance))

    while not terminate:
        action = model.infer(obs0, current_anoise)
        obs1, reward, terminate, info = env.step(action)
        # model.update_obs_normalizer(obs1)
        memory.store(obs0=obs0, obs1=obs1, action=action, reward=reward, terminate=terminate)
        obs0 = obs1
        reward_sum += reward

    critic_loss_mean = 0
    actor_loss_mean = 0
    queue.append(reward_sum)
    for j in range(30):
        batch = memory.batch(128)
        critic_loss, actor_loss, values = model.train(**batch, critic_lr=1e-4, need_target=True)
        critic_loss_mean = (critic_loss_mean * j + critic_loss) / (j + 1)
        actor_loss_mean = (actor_loss_mean * j + actor_loss) / (j + 1)
        # memory.update(slots, values)
    print('Round %5d: l: (c: %5.3f a: %5.3f) n: (a: %5.3f p: %5.3f) r: %5.3f %5.3f' % (
        i, critic_loss_mean, actor_loss_mean, current_anoise, beta.current, reward_sum, float(np.mean(queue))))
