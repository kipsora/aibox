import aibox.tensorflow as tfbox
import tensorflow as tf

rnn = tfbox.modules.nn.FixedShapeStaticLSTMModule(layers=3, units=3)
x = tf.placeholder(tf.float32, shape=[None, 32, 3])
print(rnn.apply(x))
z = tf.placeholder(tf.float32, shape=[None, 32, 3])
print(rnn.apply(z, reuse=True))
