import numpy as np

import aibox

x = np.arange(-np.pi, np.pi, 0.0001)
y1 = np.sin(x)
y2 = np.exp(x)
image = aibox.utilities.volatile.plotlib.plot_two_xy(x, y1, y2, show=True, x_label='Frequency', y1_label='Sin', y2_label='Exp', title='FFFF')
