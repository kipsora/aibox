import numpy as np

from aibox.utilities.memories.auxiliaries import Heap


def test_heap():
    n = 1000
    seed = np.random.randint(0, 65536)
    # seed = 54345
    print('seed', seed)
    heap = Heap(n)
    np.random.seed(seed)

    data = np.full((n,), -np.inf, dtype=np.float32)
    for i in range(int(n / 3 * 2)):
        prob = np.random.rand()
        heap.add(prob, i)
        data[i] = prob

    ranks = np.asarray(np.random.choice(int(n / 3 * 2), n >> 1, replace=False)) + 1
    probs, slots = heap.get_from_addr(ranks)
    data[slots] = np.random.rand(n >> 1)

    for slot, value in zip(slots, data[slots]):
        heap.update(slot, value)
    heap.reconstruct()

    for i in range(n):
        # print(i)
        assert heap.get_from_slot(i)[0] == data[i]

    for i in range(n >> 2):
        # heap.debug()
        prob, slot = heap.pop(np.random.randint(heap.size) + 1)
        assert data[slot] == prob


    last = np.inf
    while not heap.empty():
        # heap.debug()
        prob, slot = heap.pop()
        assert data[slot] == prob, (data[slot], prob)
        assert last >= prob
        last = prob


test_heap()
